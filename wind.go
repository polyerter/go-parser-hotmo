package main

import (
	"github.com/tadvi/winc"
	"time"
)

func wind() {
	window := NewWindow()

	PARSER.SetWindow(window)

	window.edt_path.SetText("C:\\tmp\\music")

	window.playlist_item.OnSelectedChange().Bind(func(e *winc.Event) {
		el := window.playlist_item.SelectedItem()
		d := el.(Playlist)

		url := d.Url
		if url != "" {
			PARSER.GetWindow().Clear()
			PARSER.GetPages().SetUrl(url)
			go PARSER.Paginate(url)
			time.Sleep(time.Second)
			window.StartGetTracks()
		}
	})

	window.btn_get_track.OnClick().Bind(func(e *winc.Event) {
		url := window.links.Text()
		if url != "" {
			PARSER.GetWindow().Clear()
			PARSER.GetPages().SetUrl(url)
			window.StartGetTracks()
		}
	})

	/*Select folder*/
	window.btn_path.OnClick().Bind(func(e *winc.Event) {
		if filePath, ok := winc.ShowBrowseFolderDlg(window.form, "folder"); ok {
			window.edt_path.SetText(filePath)
		}
	})

	/*Save files*/
	window.btn_download.OnClick().Bind(func(e *winc.Event) {
		filePath := ""

		window.log.Show()

		dl_path := window.edt_path.Text()

		/*select path*/
		if dl_path != "" {
			filePath = dl_path
			ensureDir(filePath)
		}

		items := window.tracklist.SelectedItems()
		countTracks := len(items)

		prs := make(chan int)
		window.BarView(true)

		go DL_files(items, window, filePath, prs)

		go func() {
			for {
				n, _ := <-prs

				window.SetProgressBar(countTracks, n+1)
			}
		}()
	})

	/*btn select all*/
	window.btn_select.OnClick().Bind(func(e *winc.Event) {
		window.tracklist.SetSelectedIndex(-1)
	})

	/*btn clear list*/
	window.btn_clear.OnClick().Bind(func(e *winc.Event) {
		PARSER.GetWindow().Clear()
	})

	/*btn stop*/
	window.btn_stop.OnClick().Bind(func(e *winc.Event) {
		PARSER.GetWindow().Stop()
	})

	/*btn search*/
	window.search_btn.OnClick().Bind(func(e *winc.Event) {
		PARSER.GetWindow().Clear()

		text := PARSER.GetWindow().search_input.Text()

		url := PARSER.SetSearchUrl(text)

		PARSER.GetPages().SetUrl(url)

		go PARSER.Paginate(url)

		time.Sleep(time.Second)
		window.StartGetTracks()
	})

	/*end action*/
	window.form.Center()
	window.form.Show()
	window.form.OnClose().Bind(wndOnClose)

	winc.RunMainLoop() // Must call to start event loop.
}

func DL_files(items []winc.ListItem, w *Window, filePath string, num chan<- int) {
	if filePath == "" {
		w.AddLog("error: file path empty")
		return
	}

	ch := make(chan string)

	for i, it := range items {
		el := it.(*Item)

		w.AddLog("start: " + el.song.trackname())

		go downloadFile(filePath, el.song, ch)

		time.Sleep(time.Second + 5)

		r := <-ch
		if r != "" {
			num <- i
		}

		w.AddLog(r)
	}
}

func percent(a int, b int) int {
	if a == 0 || b == 0 {
		return 0
	}

	return 100 * b / a
}

func wndOnClose(arg *winc.Event) {
	winc.Exit()
}
