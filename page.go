package main

import (
	"net/url"
	"strings"
)

type Pages struct {
	url   string
	Items []string
}

func (box *Pages) AddPage(item string) {
	if !box.isset(item) {
		box.Items = append(box.Items, item)
	}
}

func (pages Pages) first() string {
	return pages.Items[0]
}

func (pages *Pages) Clear() {
	pages.Items = nil
}

func (pages *Pages) count() int {
	return len(pages.Items)
}

func (p *Pages) SetUrl(url string) {
	p.url = url
}

func (p *Pages) GetUrl() string {
	if len(p.url) > 0 {
		return p.url
	}

	return ""
}

func (p *Pages) parseUrls() {
	urls := strings.Split(p.GetUrl(), "\r\n")

	for _, u := range urls {
		if u != "" {
			if isValidUrl(u) {
				PARSER.GetPages().AddPage(u)
			}
		}
	}
}

// isValidUrl tests a string to determine if it is a well-structured url or not.
func isValidUrl(toTest string) bool {
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false
	}

	u, err := url.Parse(toTest)
	if err != nil || u.Scheme == "" || u.Host == "" {
		return false
	}

	return true
}

func (p *Pages) isset(u string) bool {
	for _, it := range p.Items {
		if it == u {
			return true
		}
	}
	return false
}
