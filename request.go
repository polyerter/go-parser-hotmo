package main

import (
	"net/http"
)

func getBody(url string) (*http.Response, error) {
	// Request the HTML page.
	res, err := http.Get(url)

	//fmt.Println(res)

	if err != nil {
		return nil, nil
	}

	if res.StatusCode != 200 {
		return nil, nil
	}

	return res, nil
}
