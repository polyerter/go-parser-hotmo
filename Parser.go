package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"io/ioutil"
	"os"
	"strings"
)

type IParser interface {
	Tracklist(doc *goquery.Document, f chan<- bool)
	Title() string
	BaseUrl() string
	GetPlaylist() *[]Playlist
	AddPlaylist(*Playlist)

	Paginate(string) []string
	SetSearchUrl(string) string

	SetWindow(*Window)
	GetWindow() *Window

	SetDocs(*Document)
	GetDocs() *Document

	SetPages(*Pages)
	GetPages() *Pages

	SetTrackList(*TracksList)
	GetTrackList() *TracksList

	SetCTs(*CTs)
	GetCTs() *CTs
}

type Playlist struct {
	Title string `json:"title"`
	Url   string `json:"url"`
}

var f_playlist = "_playlist.txt"

func (p Playlist) Text() string {
	return p.Title
}

func (Playlist) ImageIndex() int {
	return 0
}

func (p Playlist) toJson() string {
	return "{\"title\":\"" + p.Title + "\",\"url\":\"" + p.Url + "\"},"
}

func (p *Playlist) write() {
	if _, err := os.Stat(f_playlist); os.IsNotExist(err) {
		_, err := os.Create(f_playlist)

		if err != nil {
			fmt.Println(err)
			return
		}
	}

	if p.find() {
		return
	}

	f, err := os.OpenFile(f_playlist, os.O_APPEND|os.O_WRONLY, 0644)

	if err != nil {
		fmt.Println(err)
		return
	}

	l, err := fmt.Fprintln(f, p.toJson())
	if err != nil {
		fmt.Println(err)
		f.Close()
		return
	}

	if l == 0 {
		//fmt.Println("error")
	}

	err = f.Close()
	if err != nil {
		fmt.Println(err)
		return
	}
}

func (p *Playlist) find() bool {
	lines := fileToStrings(f_playlist)

	for _, v := range lines {
		if strings.Contains(v, p.Title) {
			return true
		}
	}

	return false
}

func fileToStrings(filename string) []string {
	bytesRead, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println(err)
		return []string{}
	}

	file_content := string(bytesRead)
	return strings.Split(file_content, ",\n")
}
