package main

import (
	"strings"
)

type TracksList struct {
	Items []*Song
}

func (tl *TracksList) AddSong(item *Song) {
	//if !tl.isset(item) {
	tl.Items = append(tl.Items, item)
	//}
}

func (tl *TracksList) AddSongs(items []*Song) {
	for _, item := range items {
		tl.AddSong(item)
	}
}

func (tl *TracksList) count() int {
	return len(tl.Items)
}

func (tl *TracksList) toString() string {
	var items []string

	if len(tl.Items) > 0 {
		for _, item := range tl.Items {
			items = append(items, item.trackname())
		}
	}

	return strings.Join(items, "\n")
}

func (tl *TracksList) Clear() {
	tl.Items = nil
}

func (tl *TracksList) PrintItems() {
	for _, item := range tl.Items {
		item.PrintSong()
	}
}

func (tl *TracksList) isset(song *Song) bool {
	for _, it := range tl.Items {
		if it.id == song.id {
			return true
		}
	}
	return false
}
