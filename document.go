package main

import (
	"github.com/PuerkitoBio/goquery"
)

type Document struct {
	Items []goquery.Document
}

func (box *Document) AddDocument(item *goquery.Document) {
	box.Items = append(box.Items, *item)
}

func (box Document) first() goquery.Document {
	if len(box.Items) > 0 {
		return box.Items[0]
	}

	return goquery.Document{}
}

func (box *Document) Clear() {
	box.Items = nil
}

func request(url string, d chan<- *goquery.Document) {
	res, err := getBody(url)

	if err != nil {
		d <- nil
	}
	defer res.Body.Close()

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		d <- nil
	}

	d <- doc
}

func (d *Document) count() int {
	return len(d.Items)
}
