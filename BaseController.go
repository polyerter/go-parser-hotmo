package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
)

type BaseController struct {
	scheme   string
	baseUrl  string
	title    string
	playlist []Playlist
	wind     *Window
	pages    *Pages
	docs     *Document
	tracks   *TracksList
	cts      *CTs
}

func (p *BaseController) GetPlaylist() *[]Playlist {
	lines := fileToStrings(f_playlist)

	for _, v := range lines {

		if v != "" {
			pl := Playlist{}

			if err := json.Unmarshal([]byte(v), &pl); err != nil {
				fmt.Println("ugh: ", err)
			}

			p.playlist = append(p.playlist, pl)
		}
	}

	return &p.playlist
}

func (p *BaseController) AddPlaylist(playlist *Playlist) {
	// add base url
	playlist.Url = p.BaseUrl() + playlist.Url

	//p.playlist = append(p.playlist, *playlist)

	playlist.write()
}

func (p *BaseController) Title() string {
	return p.title
}

func (p *BaseController) BaseUrl() string {
	return p.scheme + "://" + p.baseUrl
}

func (c *BaseController) Tracklist(doc *goquery.Document, song chan []Song) {

}

func (c *BaseController) SetWindow(w *Window) {
	c.wind = w
}

func (c *BaseController) GetWindow() *Window {
	return c.wind
}

func (c *BaseController) SetDocs(w *Document) {
	c.docs = w
}

func (c *BaseController) GetDocs() *Document {
	return c.docs
}

func (c *BaseController) SetPages(w *Pages) {
	c.pages = w
}

func (c *BaseController) GetPages() *Pages {
	return c.pages
}

func (c *BaseController) SetTrackList(w *TracksList) {
	c.tracks = w
}

func (c *BaseController) GetTrackList() *TracksList {
	return c.tracks
}

func (c *BaseController) SetCTs(w *CTs) {
	c.cts = w
}

func (c *BaseController) GetCTs() *CTs {
	return c.cts
}

func (p *BaseController) Paginate(string) []string {
	return []string{}
}

func (p *BaseController) SetSearchUrl(string) string {
	return ""
}
