package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"reflect"
)

//const limit = 1000
//const count_dl = 5

var PARSER IParser = NewHotmo()

func main() {
	start()
}

func start() {
	PARSER.SetDocs(&Document{})
	PARSER.SetPages(&Pages{})
	PARSER.SetTrackList(&TracksList{})
	PARSER.SetCTs(&CTs{})

	PARSER.GetCTs().AddType(CT{"audio/mpeg", "mp3"})

	wind()
}

func reset() {
	PARSER.GetPages().Clear()
	PARSER.GetDocs().Clear()
	PARSER.GetTrackList().Clear()
}

func downloadFile(folder string, song Song, ch chan string) {
	song.SetFolder(folder)

	//Get the response bytes from the url
	response, err := http.Get(song.url)
	if err != nil {
		fmt.Println("error")
		ch <- "error"
		return
	}
	defer response.Body.Close()

	//Create a empty file
	if _, err := os.Stat(song.savepath()); os.IsNotExist(err) {
		// path/to/whatever does not exist

		file, err := os.Create(song.savepath())
		if err != nil {
			fmt.Println("error")
			return
		}
		defer file.Close()

		//Write the bytes to the fiel
		_, err = io.Copy(file, response.Body)
		if err != nil {
			fmt.Println("error")
			return
		}

		ch <- "finish: " + song.trackname()
	} else {
		ch <- "repeat track: " + song.trackname()
	}
}

func ensureDir(dirName string) string {
	if _, serr := os.Stat(dirName); serr != nil {
		merr := os.MkdirAll(dirName, os.ModePerm)
		if merr != nil {
			panic(merr)
		}
	}

	return dirName
}

func typeof(v interface{}) string {
	return reflect.TypeOf(v).String()
}
