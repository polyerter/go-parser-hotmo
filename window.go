package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tadvi/winc"
	"time"
)

const (
	wSize = 800
	hSize = 700
	xPos  = 10
	yPos  = 20
)

type Window struct {
	form *winc.Form

	btn_get_track *winc.PushButton
	btn_download  *winc.PushButton
	btn_select    *winc.PushButton

	edt_path *winc.Edit
	btn_path *winc.PushButton

	btn_clear *winc.PushButton

	search_input *winc.Edit
	search_btn   *winc.PushButton

	progressbar *winc.ProgressBar
	btn_stop    *winc.PushButton

	tracklist *winc.ListView

	links *winc.MultiEdit

	menu     *winc.Menu
	menuItem *winc.MenuItem

	playlist_select *winc.ScrollView
	playlist_item   *winc.TreeView

	log *winc.ListView
}

type Item struct {
	T       []string
	song    Song
	checked bool
}

type LogItem struct {
	T []string
}

func (item Item) Text() []string  { return item.T }
func (item Item) ImageIndex() int { return 0 }

func (item LogItem) Text() []string  { return item.T }
func (item LogItem) ImageIndex() int { return 0 }

func NewWindow() *Window {
	w := Window{}

	w.form = winc.NewForm(nil)
	w.form.SetSize(wSize, hSize) // (width, height)
	w.form.SetText("Parse " + PARSER.Title())

	/*menu*/
	w.menu = w.form.NewMenu()
	w.menuItem = w.menu.AddSubMenu("File")
	w.menuItem.AddItem("New", winc.NoShortcut)
	hotmo := w.menuItem.AddItem("HOTMO", winc.Shortcut{})
	vkaudio := w.menuItem.AddItem("Audio-VK4", winc.Shortcut{})

	/*btn get track list*/
	w.btn_get_track = winc.NewPushButton(w.form)
	w.btn_get_track.SetText("Get tracks")
	w.btn_get_track.SetPos(xPos+30, yPos) // (xPos, yPos)

	/*input url*/
	url := ""

	/*url := "https://ruq.hotmo.org/songs/top-rated\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/48\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/96\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/144\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/192\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/240\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/288\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/336\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/384\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/432\r\n" +
	"https://ruq.hotmo.org/songs/top-rated/start/480"*/

	w.links = winc.NewMultiEdit(w.form)
	w.links.SetPos(xPos, yPos*3+5)
	w.links.AddLine(url)
	w.links.SetSize(250, 150)

	/*progress bar*/
	w.progressbar = winc.NewProgressBar(w.form)
	w.progressbar.SetPos(xPos*30, yPos)
	w.progressbar.Hide()

	w.btn_stop = winc.NewPushButton(w.form)
	w.btn_stop.SetText("stop")
	w.btn_stop.SetPos(xPos*30+200, yPos-5)
	w.btn_stop.SetSize(50, 22)
	w.btn_stop.Hide()

	/*tracklist*/
	w.tracklist = winc.NewListView(w.form)
	w.tracklist.SetCheckBoxes(true)
	w.tracklist.SetPos(xPos*30, yPos*4)
	w.tracklist.SetSize(350, 250)
	w.tracklist.AddColumn("Name", 280)
	w.tracklist.AddColumn("Time", 50)
	w.tracklist.Show()

	/*btn download*/
	w.btn_download = winc.NewPushButton(w.form)
	w.btn_download.SetText("Download selected")
	w.btn_download.SetPos(xPos*30+120, yPos) // (xPos, yPos)
	w.btn_download.Hide()

	/*btn select all tracks*/
	w.btn_select = winc.NewPushButton(w.form)
	w.btn_select.SetText("Select All")
	w.btn_select.SetPos(xPos*30, yPos) // (xPos, yPos)
	w.btn_select.Hide()

	/*input edit path*/
	w.edt_path = winc.NewEdit(w.form)
	w.edt_path.SetText("")
	w.edt_path.SetPos(xPos*30, yPos*3-5)
	w.edt_path.Show()

	w.btn_path = winc.NewPushButton(w.form)
	w.btn_path.SetText("folder")
	w.btn_path.SetPos(xPos*30+200, yPos*3-5)
	w.btn_path.SetSize(50, 22)
	w.btn_path.Show()

	w.btn_clear = winc.NewPushButton(w.form)
	w.btn_clear.SetText("Clear")
	w.btn_clear.SetPos(xPos*30+250, yPos*3-5)
	w.btn_clear.SetSize(50, 22)
	w.btn_clear.Show()

	/*input search*/
	w.search_input = winc.NewEdit(w.form)
	w.search_input.SetText("")
	w.search_input.SetPos(xPos, yPos*3+5+180)
	w.search_input.Show()

	w.search_btn = winc.NewPushButton(w.form)
	w.search_btn.SetText("Найти")
	w.search_btn.SetPos(xPos+202, yPos*3+4+180)
	w.search_btn.SetSize(50, 22)
	w.search_btn.Show()

	hotmo.OnClick().Bind(func(e *winc.Event) {
		PARSER = NewHotmo()
		start()
	})

	vkaudio.OnClick().Bind(func(e *winc.Event) {
		//PARSER = NewYandex()
		start()
	})

	w.menu.Show()

	/*tracklist*/
	w.log = winc.NewListView(w.form)
	w.log.SetCheckBoxes(true)
	w.log.SetPos(xPos*30, yPos*3+280)
	w.log.SetSize(250, 100)
	w.log.AddColumn("msg", 238)
	w.log.Show()

	w.playlist_select = winc.NewScrollView(w.form)
	w.playlist_select.SetPos(xPos, yPos+280)

	w.playlist_item = winc.NewTreeView(w.playlist_select)
	w.playlist_item.SetSize(150, 100)

	w.playlist_select.SetChild(&w.playlist_item.ControlBase)
	for _, pl := range *PARSER.GetPlaylist() {
		_ = w.playlist_item.InsertItem(pl, nil, nil)
	}

	return &w
}

func (w *Window) BarView(status bool) {
	if status {
		w.btn_download.Hide()
		w.btn_select.Hide()
		w.progressbar.Show()
		w.btn_stop.Show()
	} else {
		w.btn_download.Show()
		w.btn_select.Show()
		w.progressbar.Hide()
		w.btn_stop.Hide()
	}
}

func (w *Window) StartGetTracks() {
	fmt.Println("working...")

	w.BarView(true)

	w.tracklist.Show()

	w.AddLog("get track list")

	PARSER.GetPages().parseUrls()

	qqqq()

	time.Sleep(time.Second)

	go w.FinishPrintTrackList()
}

func qqqq() {
	f := make(chan bool)
	doc := make(chan *goquery.Document, PARSER.GetPages().count())
	url := make(chan string)

	go func(uri chan string) {
		for u := range uri {
			go request(u, doc)
			//fmt.Println("request", u)

			go func() {
				for el := range doc {
					//fmt.Println("doc")
					PARSER.GetDocs().AddDocument(el)
					PARSER.Tracklist(el, f)
				}
			}()
		}
	}(url)

	go func() {
		for _, it := range PARSER.GetPages().Items {
			url <- it
			//fmt.Println("Items", it)
		}
		close(url)
	}()
	time.Sleep(time.Second)
}

func (w *Window) AddLog(msg string) {
	w.log.AddItem(&LogItem{T: []string{msg}})
}

func (w *Window) SetProgressBar(a int, b int) {
	v := percent(a, b)
	w.progressbar.SetValue(v)

	if v >= 97 {
		w.BarView(false)
	}
}

func (w *Window) AddSongTrackList(song Song) {
	timet := song.GetTimeString()
	item := &Item{[]string{song.trackname(), timet}, song, false}

	w.tracklist.AddItem(item)
}

func (w *Window) Clear() {
	w.tracklist.DeleteAllItems()
	w.btn_path.Show()
	w.edt_path.Show()
	w.log.Show()
	reset()
}

func (w *Window) Stop() {
	start()
}

func (w *Window) FinishPrintTrackList() {
	w.AddLog(fmt.Sprintf("Count page: %d", PARSER.GetPages().count()))
	w.AddLog(fmt.Sprintf("Count tracks: %d", PARSER.GetTrackList().count()))
	PARSER.GetTrackList().PrintItems()
	PARSER.GetWindow().BarView(false)
}
