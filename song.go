package main

import (
	"fmt"
	"strconv"
)

type ISong interface {
	trackname() string
	savepath() string
}

type Song struct {
	id       string
	title    string
	artist   string
	format   string
	url      string
	img      string
	playlist string
	folder   string
	bitrate  int
	time     int
	size     int
}

func NewSong(id string, title string, artist string, url string, img string, format string, playlist string, folder string, bitrate int, time int, size int) *Song {
	s := Song{id: id, title: title, artist: artist, url: url, img: img, format: format, playlist: playlist, folder: folder, bitrate: bitrate, time: time, size: size}

	s.SetFormat(PARSER.GetCTs().Detect(s.format))

	if bitrate == 0 {
		if size != 0 && time != 0 {
			s.CalcBitrate()
		}
	}

	return &s
}

func (s Song) trackname() string {
	return fmt.Sprintf("%s - %s.%s", s.artist, s.title, s.format)
}

func (s Song) savepath() string {
	return fmt.Sprintf("%s/%s", s.folder, s.trackname())
}

func (s *Song) SetFolder(folder string) {
	s.folder = folder
}

func (s *Song) SetFormat(val string) {
	s.format = val
}

func (s *Song) GetTimeString() string {
	time := s.time

	min := formatTimeToString(time / 60)
	sec := formatTimeToString(time % 60)

	return min + ":" + sec
}

func (s *Song) CalcBitrate() {
	a := s.size * 8
	s.bitrate = a / s.time
}

func formatTimeToString(time int) string {
	t1 := strconv.Itoa(time % 60)

	if len(t1) == 1 {
		t1 = "0" + t1
	}

	return t1
}

func (s *Song) PrintSong() {
	PARSER.GetWindow().AddSongTrackList(*s)
}
