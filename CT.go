package main

type CT struct {
	mime string
	name string
}

type CTs struct {
	Items []CT
}

func (cts *CTs) Detect(item string) string {
	for _, it := range cts.Items {
		if it.mime == item {
			return it.name
		}
	}

	return item
}

func (cts *CTs) AddType(item CT) {
	cts.Items = append(cts.Items, item)
}
