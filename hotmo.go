package main

import (
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"strconv"
	"strings"
)

type Hotmo struct {
	BaseController
}

// Constructor
func NewHotmo() *Hotmo {
	p := Hotmo{}

	p.title = "HOTMO"
	p.scheme = "https"
	p.baseUrl = "ruv.hotmo.org"

	f_playlist = p.title + "_playlist.txt"

	p.AddPlaylist(&Playlist{Title: "Популярные песни", Url: "/songs/top-rated"})
	p.AddPlaylist(&Playlist{"Иностранный рэп", "/genre/99"})
	p.AddPlaylist(&Playlist{"Русский рэп", "/genre/75"})
	p.AddPlaylist(&Playlist{"Шансон", "/genre/14"})
	p.AddPlaylist(&Playlist{"Dance", "/genre/11"})
	p.AddPlaylist(&Playlist{"Подкасты саморазвитие", "/genre/165"})
	p.AddPlaylist(&Playlist{"POP", "/genre/96"})

	return &p
}

func (p Hotmo) Tracklist(doc *goquery.Document, f chan<- bool) {
	data := []*Song{}

	items := doc.Find(".tracks__list .track")
	// Find the review items
	items.Each(func(i int, s *goquery.Selection) {
		dataJson, exist := s.Attr("data-musmeta")
		if exist != true {
			print(exist)
		}

		e := make(map[string]string)

		err := json.Unmarshal([]byte(dataJson), &e)

		if err != nil {

		}

		time1 := timeToSecond(s.Find(".track__fulltime").Text())

		sss := NewSong(e["id"], e["title"], e["artist"], e["url"], e["img"], "audio/mpeg", "", "", 0, time1, 0)

		data = append(data, sss)
	})

	PARSER.GetTrackList().AddSongs(data)

	f <- true
}

func timeToSecond(qtime string) int {
	qtimes := strings.Split(qtime, ":")
	time1, _ := strconv.Atoi(qtimes[0])
	time2, _ := strconv.Atoi(qtimes[1])

	return time1*60 + time2
}

func (p *Hotmo) Paginate(uri string) []string {
	var urls []string
	url := PARSER.BaseUrl()

	doc := make(chan *goquery.Document)
	go request(uri, doc)

	urls = append(urls, url)
	//PARSER.GetPages().AddPage(url)

	go func() {
		for el := range doc {
			paginate, _ := el.Find(".pagination__list .pagination__link").Last().Attr("href")

			if paginate != "" {
				q := ""
				pp := strings.Split(paginate, "?")
				if len(pp) == 2 {
					q = "?" + pp[1]
					paginate = pp[0]
					uri = strings.Replace(uri, q, "", -1)
				}

				page := strings.Split(paginate, "/")

				p := page[len(page)-1]

				count, _ := strconv.ParseInt(p, 10, 32)

				for i := 48; i < int(count)+48; i = i + 48 {
					u := fmt.Sprintf("%s/start/%d%s", uri, i, q)
					urls = append(urls, u)

					PARSER.GetPages().AddPage(u)
				}
			}
		}
	}()

	return urls
}

func (p *Hotmo) SetSearchUrl(text string) string {
	return PARSER.BaseUrl() + "/search?q=" + text
}
